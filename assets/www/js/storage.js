var storage = {
	getDb: function() {
	    try {
	        if( !! window.localStorage ) return window.localStorage;
	    } catch(e) {
	        return undefined;
	    }
	},
	getUrl: function() {
		return storage.getDb().getItem("url");
	},
	getApiKey: function() {
		return storage.getDb().getItem("apiKey");
	},
	setUrl: function(url) {
		storage.getDb().setItem("url", url);
	},
	setApiKey: function(apiKey) {
		storage.getDb().setItem("apiKey", apiKey);
	}
}