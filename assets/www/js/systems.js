var url = storage.getUrl();
var apiKey = storage.getApiKey();

$('#systemsSection').live('pageshow',function(event, ui){
	setTimeout("hideSplash()", 1500);
});

function hideSplash() {
	$('#splash').css('display', 'none');
	$('#systemsSection').removeClass('splash');
	
	if ((url == null) || (url == "")) {
		$('#systems').css('display', 'none');
		$('#preferences').removeAttr('style');
	} else {
		$('#systems').removeAttr('style');
		if (!systemBean.loaded) {
			systemBean.fetch();
		}
	}
}

var systemBean = {
	loaded: false,
	fetch: function() {
		$.ajax({
	        beforeSend: function() { $.mobile.showPageLoadingMsg(); }, //Show spinner
	        complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
	        url: storage.getUrl() + '/systems',
	        data: {'key' : storage.getApiKey()},
	        dataType: 'jsonp',
	        jsonp : 'callback',
	        success: function(data) {
	            var systems = data.systems;
				var listItems = '';
				for (var i=0; i < systems.length; i++) {
					var listItem = '<li><a href="#details?id=' + systems[i].system_id + 
							'&name=' + systems[i].system_name + '">';
					var name = '<div><img src="images/' + systems[i].status + '_small.png" />&nbsp;' + systems[i].system_name + '</div>';
					var addressValue = systems[i].city + ", " + systems[i].state + " " + systems[i].postal_code;
					var address = '<div style="font-size: 11px"><span style="color: orange;">Address:</span> ' + addressValue + '</div>';
					var timezone = '<div style="font-size: 11px"><span style="color: orange;">Timezone:</span> ' + systems[i].timezone + '</div>';	
					listItem = listItem + name + address + timezone + '</a></li>';
					listItems = listItems + listItem;
				};
				$("#systemsList").html(listItems);
				$("#systemsList").listview("refresh");
				systemBean.loaded = true;
	        }
	    });
	}
}



$('#systemsSection').live('pageinit',function(event, ui){
	
	$("#url").val(url);
	$("#apiKey").val(apiKey);
	
	$("#systemsRefresh").click(function() {
		systemBean.fetch();
	});
	
	$("#prefsForm").validate({
	    submitHandler: function(form) {
	    	storage.setUrl($("#url").val());
	    	storage.setApiKey($("#apiKey").val());
	    	$('#preferences').css('display', 'none');
			$('#systems').removeAttr('style');
			systemBean.fetch();
	    }
	});
	
	$("#prefsSave").click(function(evt) {
		evt.preventDefault();
		$("#prefsForm").submit();
	});
	
	$("#prefsButton").click(function(evt) {
		$('#systems').css('display', 'none');
		$('#preferences').removeAttr('style');
	});
	
});



