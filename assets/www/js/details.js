var global = {
	id: "",
	name: "",
	calendarOpen: false
}

var summary = {
	loaded: false,
	fetch: function() {
		$.ajax({
	        beforeSend: function() { $.mobile.showPageLoadingMsg(); }, //Show spinner
	        complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
	        url: storage.getUrl() + '/systems/' + global.id + '/summary',
	        data: {'key' : storage.getApiKey()},
	        dataType: 'jsonp',
	        jsonp : 'callback',
	        success: function(data) {
	            var power = '<tr><td>Power Production:</td><td>' + calculateKiloWattHours(data.current_power) + '</td></tr>';
				var today = "<tr><td>Today's Energy:</td><td>" + calculateKiloWattHours(data.energy_today) + '</td></tr>';
				var seven = '<tr><td>Past 7 Days:</td><td>' + calculateKiloWattHours(data.energy_week) + '</td></tr>';
				var month = "<tr><td>This Month's:</td><td>" + calculateKiloWattHours(data.energy_month) + '</td></tr>';
				var lifetime = '<tr><td>Lifetime Energy:</td><td>' + calculateKiloWattHours(data.energy_lifetime) + '</td></tr>';
				var inverters = '<tr><td>Inverters:</td><td>' + data.modules + '</td></tr>';
					
				var content = power + today + seven + month + lifetime + inverters; 
				$("#summaryList").html(content);
				$("tr td").css("font-size", "15px");
				//$('#summaryList td:eq(1)').css("color", "orange");
				$('#summaryList tr').each(function() {
				    var customerId = $(this).find("td").eq(1).css("color", "orange"); 
				});
				summary.loaded = true;
	        }
	    });
	}
}

var alerts = {
	loaded: false,
	level: 'low',
	fetch: function() {
		$.ajax({
	        beforeSend: function() { $.mobile.showPageLoadingMsg() }, //Show spinner
	        complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
	        url: storage.getUrl() + '/systems/' + global.id + '/alerts',
	        data: {'level' : this.level, 'key' : storage.getApiKey()},
	        dataType: 'jsonp',
	        jsonp : "callback",
	        success: function(data) {
	        	var alertList = data.alerts;
				var listItems = '';
				if (alertList.length == 0) {
					$("#alertsNoList").removeClass("hide");
					$("#alertsList").addClass("hide");
				} else {
					for (var i=0; i < alertList.length; i++) {
						var alertName = '<div>' + alertList[i].alert_name + '</div>';
						var devices = '<div style="font-size: 11px"><span style="color: orange;"># of devices:</span> ' + alertList[i].num_devices + '</div>';
						var alertStart = '<div style="font-size: 11px"><span style="color: orange;">First Reported:</span> ' + alertList[i].alert_start + '</div>';
						var listItem = '<li>' + alertName + devices + alertStart + '</li>';
						listItems = listItems + listItem;
					};
					$("#alertsList").removeClass("hide");
					$("#alertsNoList").addClass("hide");
					$("#alertsList").html(listItems);
					$("#alertsList").listview("refresh");
				}
				
				alerts.loaded = true;
	        }
	    });
	}
}

var stats = {
	loaded: false,
	currentDate: "",
	selectedDate: "",
	start: "",
	end: "",
	offset: "",
	watts: [],
	time: [],
	width: 0,
	//params: function() {
	//	if (stats.currentDate == $("#statsDate").val()) {
	//		return {'key' : storage.getApiKey()};
	//	} else {
	//		return {'key' : storage.getApiKey(), 'start' : stats.start, 'end' : stats.end};
	//	}
	//},
	params: function() {
		if (stats.currentDate == $("#statsDate").val()) {
			return '?key=' + storage.getApiKey();
		} else {
			return '?key=' + storage.getApiKey() + '&start=' + stats.start + '&end=' + stats.end;
		}
	},
	setDate: function(date) {
		
		stats.selectedDate = date;
		
		var dateArray=date.split("/");
		if (dateArray[0].length < 2) {
			dateArray[0] = "0" + dateArray[0];
		}
		if (dateArray[1].length < 2) {
			dateArray[1] = "0" + dateArray[1];
		}
		stats.start = dateArray[2] + "-" + dateArray[0] + "-" + dateArray[1] + "T00:01-" + stats.offset;
		stats.end = dateArray[2] + "-" + dateArray[0] + "-" + dateArray[1] + "T23:59-" + stats.offset;
		//stats.start = dateArray[2] + "-" + dateArray[0] + "-" + dateArray[1];
		//stats.end = dateArray[2] + "-" + dateArray[0] + "-" + dateArray[1];
	},
	fetch: function() {
		$.ajax({
	        beforeSend: function() { $.mobile.showPageLoadingMsg(); }, //Show spinner
	        complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
	        url: storage.getUrl() + '/systems/' + global.id + '/stats' + stats.params(),
	        //data: stats.params(),
	        dataType: 'jsonp',
	        jsonp : 'callback',
	        contentType: 'application/x-www-form-plaintext',
	        success: function(data) {
				var intervals = data.intervals;
				var listItems = '';
				var previousHour = '';
				var count = 0;
				var listItem = '';
				var divider = '';
				var totalPower = 0;
				var totalWatts = 0;
				
				stats.watts = new Array(intervals.length);
				stats.time = new Array(intervals.length);
				
				for (var i=intervals.length-1; i > -1; i--) {
					
					var hour = intervals[i].end_date.substring(11,13);
					if ((hour != previousHour) && (previousHour != '')) {
						divider = '<li data-role="list-divider">' + formatHourlyTime(previousHour) + '<span class="ui-li-count">' + count + '</span></li>';
						listItems = listItems + divider + listItem;
						count = 0;
						listItem = '';
					}
					previousHour = hour;
					count++;
					
					totalPower = totalPower + intervals[i].powr;
					totalWatts = totalWatts + intervals[i].enwh;
					
					var formattedTime = formatTime(intervals[i].end_date.substring(11,16));
					stats.watts[i] = intervals[i].enwh;
					stats.time[i] = formattedTime;
					
					listItem = listItem + '<li>';
					var devices = '<p>Inverters Reporting: ' + intervals[i].devices_reporting + '</p>';
					var endDate = '<p class="ui-li-aside">' + formattedTime + '</p>';
					var powr = '<p style="font-size: 11px"><span style="color: orange;">Power in watts:</span> ' + calculateKiloWattHours(intervals[i].powr) + '</p>';
					var enwh = '<p style="font-size: 11px"><span style="color: orange;">Energy in watt hours:</span> ' + calculateKiloWattHours(intervals[i].enwh) + '</p>';
					listItem = listItem + devices + endDate + powr + enwh + '</li>';
					//listItems = listItems + listItem;
				};
				if (intervals.length == 0) {
					listItems = '<li>No Stats</li>'
				} else {
					divider = '<li data-role="list-divider">' + formatHourlyTime(hour) + '<span class="ui-li-count">' + count + '</span></li>';
					listItems = listItems + divider + listItem;
					
					// create total line
					divider = '<li data-role="list-divider">Total</li>';
					listItem = '<li>';
					var powr = '<p style="font-size: 11px"><span style="color: orange;">Power in watts:</span> ' + calculateKiloWattHours(totalPower) + '</p>';
					var enwh = '<p style="font-size: 11px"><span style="color: orange;">Energy in watt hours:</span> ' + calculateKiloWattHours(totalWatts) + '</p>';
					listItem = listItem + powr + enwh + '</li>';
					listItems = divider + listItem + listItems;
				}
				$("#statsList").html(listItems);
				$("#statsList").listview("refresh");
				
				$("#sparkline").css("width", stats.width + 'px');
				$("#sparkline").sparkline(stats.watts, {
					type : 'line',
					width : stats.width + 'px',
					height : '40px',
					fillColor : '#0000f0',
					spotColor : '#0000f0',
					minSpotColor : '#0000f0',
					maxSpotColor : '#0000f0',
					highlightSpotColor : '#0000f0',
					highlightLineColor : '#0000f0',
					enableTagOptions : false
				});

				stats.loaded = true;
	        }
	    });
	}
}

$('#details').live('pageshow',function(event, ui){
	global.id = getParameter("id");
	global.name = decodeURIComponent(getParameter("name"));
	$('#headerName').html(global.name);
	
	summary.fetch();
});

// intialize page
$('#details').live('pageinit',function(event, ui){
	
	summary.loaded = false;
	alerts.loaded = false;
	stats.loaded = false;
	
	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var offset = currentDate.getTimezoneOffset() / 60;
	if (offset < 10) {
		stats.offset = "0" + offset + ":00";
	} else {
		stats.offset = offset + ":00";
	}
	var date = month + "/" + day + "/" + year;
	$("#statsDate").val(date);
	stats.setDate(date);
	stats.currentDate = date;
	stats.width = $("#stats").width() - 15;
	console.log(stats.width);
	
	$("#detailsRefresh").click(function() {
		
		if (!$('#summary').hasClass('hide')) {
			$('#summaryList').html('');
			summary.fetch();
		}
		if (!$('#alerts').hasClass('hide')) {
			$('#alertsContainer').html('');
			$("#alertsNoList").addClass("hide");
			alerts.fetch();
		}
		if (!$('#stats').hasClass('hide')) {
			$('#statsList').html('');
			$('#sparkline').html('');
			stats.fetch();
		}
	});
	
	$("#summaryLink").click(function() {
		$('#summary').removeClass('hide');
		$('#alerts').addClass('hide');
		$('#stats').addClass('hide');
		$('#chart').addClass('hide');
		if (!summary.loaded) {
			summary.fetch();
		}
	});
	
	$("#details").bind("swipeleft", function(event, ui) {
		if (!$('#summary').hasClass('hide')) {
			$("#statsLink").trigger('click');
		} else if (!$('#stats').hasClass('hide')) {
			$("#alertsLink").trigger('click');
		}	
	});
	
	$("#details").bind("swiperight", function(event, ui) {
		if (!$('#stats').hasClass('hide')) {
			$("#summaryLink").trigger('click');
		} else if (!$('#alerts').hasClass('hide')) {
			$("#statsLink").trigger('click');
		}	
	});
	
	$("#alertsLink").click(function() {
		$('#summary').addClass('hide');
		$('#alerts').removeClass('hide');
		$('#stats').addClass('hide');
		$('#chart').addClass('hide');
		if (!alerts.loaded) {
			alerts.fetch();
		}
		
		/*var plot1b = $.jqplot('chart1b', [stats.watts], {
			stackSeries : true,
			showMarker : false,
			seriesDefaults : {
				fill : true
			},
			axes : {
				xaxis : {
					pad : 0,
					renderer : $.jqplot.CategoryAxisRenderer,
					ticks : stats.time,
					tickOptions : {
						showGridline : false
					}
				},
				yaxis: { 
					pad : 0,
					tickOptions: { 
						showGridline: false
					}
				}
			}
		}); */
		
	    /*$('#chart1b').bind('jqplotDataHighlight', 
	        function (ev, seriesIndex, pointIndex, data) {
	            $('#info1b').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
	        }
	    );
	    
	    $('#chart1b').bind('jqplotDataUnhighlight', 
	        function (ev) {
	            $('#info1b').html('Nothing');
	        }
	    );*/
    
	});
	
	$("#statsLink").click(function() {
		$('#summary').addClass('hide');
		$('#alerts').addClass('hide');
		$('#stats').removeClass('hide');
		$('#chart').addClass('hide');
		stats.width = $("#stats").width() - 15;
		if (!stats.loaded) {
			stats.fetch();
		}
	});
	
	$("#chartLink").click(function() {
		$('#summary').addClass('hide');
		$('#alerts').addClass('hide');
		$('#stats').addClass('hide');
		$('#chart').removeClass('hide');
	});
	
	$("#alertSelect").bind("change", function(event, ui) {
		alerts.level = $("#alertSelect").val();
		alerts.fetch();
	});
	
	$('#statsDate').scroller({
        preset: 'date',
        theme: 'android-ics',
        display: 'modal',
        mode: 'mixed'
    });
    
    $('#statsDate').bind('change', function(event, ui) {
    	if (stats.selectedDate != $("#statsDate").val()) {
			stats.setDate($("#statsDate").val());
			$('#statsList').html('');
			$('#sparkline').html('');
			stats.fetch();
		}	
    });
	
});